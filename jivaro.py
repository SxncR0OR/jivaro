#!/usr/bin/python
# -*- coding:utf-8 -*-

# #################################################
#
#	Program name: Jivaro
#
#	Jivaro reduce image size of multiple pictures at once.
#	Copyright (C) 2012 Olivier Vandecasteele D. V.
#	Contact: olivier.vandecasteele@gmail.com
#
#	License: AGPL 3
#
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU Affero General Public License as
#	published by the Free Software Foundation, either version 3 of the
#	License, or (at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty
#	of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#	See the GNU General Public License for more details.
#
#	You should have received a copy of the GNU Affero General Public License
#	along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# #################################################


import sys, os, getopt, string, Image, ImageEnhance
 
class JivaroClass():
	def __init__(self):
		# initialize with default values
		self.size=800							# default size
		self.size_string='s'						# default string used in filename ; eg. for 800 pixels and string='-s' then the filename will end with -s800
		self.format_types=['JPEG','JPG','GIF','WebP']		# list of allowed types
		self.format_extentions=['jpg','jpg','gif','webp']	# list of allowed extentions ; the position in the list must be in relation with self.format_types
		self.format_index=0						# default index of type / extention
		self.format_quality=85					# default quality
		self.format_quality_string='q'				# default string used in filename ; eg. for a quality of 85 and string='-q' then the filename will end with -q85
		self.prefix='-'
		self.overwrite=False
		self.scaleup=False
		self.brightness=1.0
		self.contrast=1.0
		self.color=1.0
		
		self.optimize=True
		self.progressive=True
		self.subsampling=0

	def read_opts(self,opts):
		for option_str,value_str in opts:
			if option_str=='-h' or option_str=='--help':
				pass
			if option_str=='-p' or option_str=='--prefix_string':
				if isinstance(value_str,str):
					self.prefix=value_str
			if option_str=='--size_string':
				self.size_string=value_str
			if option_str=='--quality_string':
				self.format_quality_string=value_str
			if option_str=='-s' or option_str=='--size':
				if value_str.isdigit():
					self.size=int(value_str)
			if option_str=='-b' or option_str=='--brightness':
				self.brightness=float(value_str)
			if option_str=='-c' or option_str=='--contrast':
				self.contrast=float(value_str)
			if option_str=='--color':
				self.color=float(value_str)
			if option_str=='-f' or option_str=='--format':
				if value_str in self.format_types:
					self.format_index=self.format_types.index(value_str)
			if option_str=='-q' or option_str=='--quality':
				if value_str.isdigit():
					self.format_quality=int(value_str)
			if option_str=='--overwrite':
				self.overwrite=True
			if option_str=='--scaleup':
				self.scaleup=True
		return None
	
	def get_format_type(self):
		return self.format_types[self.format_index]
	
	def get_format_extention(self):
		return self.format_extentions[self.format_index]
	
	def get_fullfilenames_list(self,args):
		fullfilenames_list=[]
		for arg in args:
			if os.path.isfile(arg):
				pathname0,filename0=os.path.split(arg)
				file0,ext0=os.path.splitext(filename0)
				added_string=''
				if not self.prefix=='NONE':
					size_string=''
					if not len(self.size_string)==0:
						if not self.size_string=='NONE':
							size_string=self.size_string+str(self.size)
					quality_string=''
					if not len(self.format_quality_string)==0 and not self.format_quality==0:
						if not self.format_quality_string=='NONE':
							quality_string=self.format_quality_string+str(self.format_quality)
					added_string=size_string+quality_string
				if len(added_string)>0:
					added_string=self.prefix+added_string
				filename1=file0.lower()+added_string+"."+self.get_format_extention()
				fullfilename1=os.path.join(pathname0,filename1)
				if self.overwrite:
					fullfilenames_list.append( (arg,fullfilename1) )
				else:
					if fullfilename1!=arg: # never overwrite it
						fullfilenames_list.append( (arg,fullfilename1) )
		return fullfilenames_list
	
	def resize_images(self,fullfilenames):
		for fullfilename0,fullfilename1 in fullfilenames:
			img0=Image.open(fullfilename0)
			imgX, imgY = img0.size
			targetSize=(imgX,imgY)
			if self.size==0: # dont modify the size
				img1=img0
			else:
				dontscaleupFlag=False
				if max(imgX,imgY)<=self.size and self.scaleup==False:
					dontscaleupFlag=True
				if not dontscaleupFlag:
					if imgX>imgY:
						redFactor=float(imgX)/float(self.size)
						newX=int(self.size)
						newY=int((float(imgY)/redFactor)+0.5)
						targetSize=(newX,newY)
					else:
						redFactor=float(imgY)/float(self.size)
						newX=int((float(imgX)/redFactor)+0.5)
						newY=int(self.size)
						targetSize=(newX,newY)
					img1=None
					if imgX>int(newX*1.8):
						img1=img0.resize((newX*2,newY*2),Image.ANTIALIAS)
						img1=ImageEnhance.Sharpness(img1).enhance(2.0)
						img1=img1.resize(targetSize,Image.ANTIALIAS)
					else:
						img1=img0.resize(targetSize,Image.ANTIALIAS)
				else:
					img1=img0
			if self.format_index==2: #GIF
				img1=img1.convert(mode='P', palette=Image.ADAPTIVE, colors=16, dither=1)
			if not self.brightness==1.0:
				img1=ImageEnhance.Brightness(img1).enhance(self.brightness)
			if not self.contrast==1.0:
				img1=ImageEnhance.Contrast(img1).enhance(self.contrast)
			if not self.color==1.0:
				img1=ImageEnhance.Color(img1).enhance(self.color)
			img1.MAXBLOCK = img1.size[0] * img1.size[1]
			if self.format_index==0 or self.format_index==1:
				img1.save(fullfilename1, self.get_format_type(), quality=self.format_quality, optimize=self.optimize, progressive=self.progressive, subsampling=self.subsampling)
			else:
				img1.save(fullfilename1, self.get_format_type(), quality=self.format_quality)
		return None

def main():
	# parse command line options
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hs:q:f:p:b:c:", ["help","size=","quality=","format=","prefix_string=","size_string=","quality_string=","overwrite","scaleup","brightness=","contrast=","color="])
	except getopt.error, msg:
		sys.exit(2)
	else:
		if len(args)>=1:
			jivaro=JivaroClass()
			jivaro.read_opts(opts)
			jivaro.resize_images(jivaro.get_fullfilenames_list(args))

if __name__ == "__main__":
	main()
