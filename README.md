# Jivaro

## Synopsis

Jivaro réduit et compresse des images.

Jivaro agit sur une sélection d'images et les convertit en une seule opération.  
On l'utilise par exemple pour envoyer des photos par email ou encore pour les déposer sur un forum internet où la place est limitée.


## Screenshots

![Jivaro Screenshot Here](https://framagit.org/SxncR0OR/jivaro/raw/master/jivaro-interface.png "Jivaro Screenshot")


## Exemple d'utilisation

Sélectionnez toutes vos images en une seule fois, faites un clic droit, puis dans le menu contextuel choisissez *Jivaro* pour lancer l'interface.  
Jivaro se concentre sur l'efficacité: sélectionnez une taille et une compression puis lancez la conversion. C'est tout.

En option, vous pouvez aussi modifier la luminosité et le contraste des images.  
Cette dernière option est utilisée lorsque toutes vos photos sont trop sombre par exemple.  
Plus besoin dans ce cas de passer par un éditeur d'images et de les modifier une par une.


## Installation et intégration avec le clic droit du navigateur de fichier (Linux)

Les instructions qui suivent permettent d'utiliser *Jivaro* avec le navigateur de fichier *Nemo*.  
Si votre distribution utilise un autre navigateur, une manipulation similaire existe probablement (renseignez-vous).

### Menu contextuel avec Nemo

Menu contextuel de Jivaro avec l'explorateur de fichiers Nemo:  
![Jivaro Nemo context menu Here](https://framagit.org/SxncR0OR/jivaro/raw/master/jivaro-nemo-context-menu.png "Jivaro: Nemo context menu")

### Où placer les fichiers sous Linux ?  
Placez les différents fichiers aux endroits suivants:

Si vous voulez rendre le programme utilisable pour tous les utilisateurs, alors placez-les alors dans le répertoire système:  
`/usr/share/nemo/actions/`
> Note: Le répertoire système n'est acessible qu'avec les droits de l'administrateur du système.

Si vous voulez rendre le programme utilisable uniquement pour vous, alors placez-les alors dans votre répertoire *home*:  
`/home/username/.local/share/nemo/actions`
> Remplacez *username* par votre nom d'utilisateur.

**Remarques importantes:**
- Pour fonctionner, les fichiers Python (.py) doivent d'abord être rendus exécutables.
- En cas d'utilisation du répertoire système, vous devez posséder les droits d'administrateur système (*root*) pour y placer les fichiers et les rendre éxécutables.


### Comment rendre les fichiers Python exécutables ?

- Faites un clic droit sur votre fichier
- Dans le menu contextuel, choisissez *Propriétés*
- Choisir l'onglet *Permissions*
- À la ligne *Exécution*, cochez la case *Autoriser l'exécution ...*


## Jivaro en ligne de commande

Si vous le souhaitez, il est aussi possible d'utiliser Jivaro en ligne de commande, c'est à dire sans interface graphique.

| Argument court | Argument long | Valeur par défaut | Description |
| :---:          | :---:         |   :---:             | :---: |
| -h | --help | Aucun argument | Affiche l'aide. *Cette fonction reste à écrire.* |
| -s | --size | 800 | |
| -q | --quality | 85 | |
| -f | --format | | |
| -p | --prefix_string | - | |
|    | --size_string | s | |
|    | --quality_string | q |
| -b | --brightness | 1.0 | Modifie la brillance de l'image. 0: image noire. 1.0: image originale. >1.0: augmentation de la brillance. |
| -c | --contrast | 1.0 | Modifie le contraste de l'image. 0: image grise. 1.0: image originale. >1.0: augmentation du contraste. |
| | --color | 1.0 | Modifie la saturation des couleurs de l'image. 0: image en noir et blanc. 1.0: image originale. >1.0: augmentation de la saturation des couleurs. |
| | --overwrite | Sans argument | Permet d'écraser le fichier s'il existe déjà. |
| | --scaleup | Sans argument | Permet d'agrandir l'image au delà de sa taille d'origine. |


## Contact

<jivaro.oliviervandecasteele@xoxy.net>


## Licence

Jivaro est distribué sous licence:  
[GNU Affero General Public License version 3 (AGPL v3)](https://www.gnu.org/licenses/agpl-3.0.en.html)

---

Jivaro reduce image size of multiple pictures at once.  
Copyright (C) 2012  Vandecasteele Olivier D. V.  

This program is free software: you can redistribute it and/or modify  
it under the terms of the GNU Affero General Public License as  
published by the Free Software Foundation, either version 3 of the  
License, or (at your option) any later version.  

This program is distributed in the hope that it will be useful,  
but WITHOUT ANY WARRANTY; without even the implied warranty of  
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
GNU Affero General Public License for more details.  

You should have received a copy of the GNU Affero General Public License  
along with this program.  If not, see <http://www.gnu.org/licenses/>.
