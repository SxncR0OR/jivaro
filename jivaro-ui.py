#!/usr/bin/python
# -*- coding:utf-8 -*-

import pygtk
pygtk.require("2.0")
import gtk, sys, os, getopt

class JivaroUI(object):       
	def __init__(self,args):
		self.directory=sys.path[0]
		builder = gtk.Builder()
		builder.add_from_file(os.path.join(self.directory,"jivaro-ui.glade"))
		builder.connect_signals(self)
		self.window = builder.get_object("jivaro")
		self.window.show()
		# widget data
		self.SeparatorEntry = builder.get_object("SeparatorEntry")
		self.SizeEntry = builder.get_object("SizeEntry")
		self.QualityEntry = builder.get_object("QualityEntry")
		self.SeparatorCheckbutton = builder.get_object("SeparatorCheckbutton")
		self.SizeCheckbutton = builder.get_object("SizeCheckbutton")
		self.QualityCheckbutton = builder.get_object("QualityCheckbutton")
		self.OverwriteCheckbutton = builder.get_object("OverwriteCheckbutton")
		self.ScaleUpCheckbutton = builder.get_object("ScaleUpCheckbutton")
		self.SizeTreeview = builder.get_object("SizeTreeview")
		self.QualityTreeview = builder.get_object("QualityTreeview")
		self.SizeSelectedLabel = builder.get_object("SizeSelectedLabel")
		self.QualitySelectedLabel = builder.get_object("QualitySelectedLabel")
		self.BrightnessSpinbutton = builder.get_object("BrightnessSpinbutton")
		self.ContrastSpinbutton = builder.get_object("ContrastSpinbutton")
		self.ColorSpinbutton = builder.get_object("ColorSpinbutton")
		# widget state
		self.SizeTreeview.set_cursor(17)
		self.SizeSelectedLabel.set_text('Current selected size: '+self.get_size())
		self.SizeTreeview.get_selection().connect('changed', self.on_SizeTreeviewSel_changed)
		self.QualityTreeview.set_cursor(3)
		self.QualitySelectedLabel.set_text('Current selected quality: '+self.get_quality())
		self.QualityTreeview.get_selection().connect('changed', self.on_QualityTreeviewSel_changed)
		# software data
		self.SeparatorEntry_Text='-'
		self.SizeEntry_Text='s'
		self.QualityEntry_Text='q'
		self.args=args
		# verbose levels:
		# 0: errors and important messages
		# 1: usefull informations
		# 2: debug level
		self.verboseLevel=1
	
	def message(self,text,level=0):
		if isinstance(text,str):
			if level<=self.verboseLevel:
				print(text)
	
	def on_jivaro_destroy(self, widget):
		self.message('Window closed',level=1)
		self.message('Quit Jivaro',level=1)
		gtk.main_quit()
	
	def get_size(self):
		(SizeTreeview0,SizeTreeview1)=self.SizeTreeview.get_selection().get_selected()
		SizeStr=str(SizeTreeview0.get_value(SizeTreeview1,0))
		return SizeStr
	
	def get_quality(self):
		(QualityTreeview0,QualityTreeview1)=self.QualityTreeview.get_selection().get_selected() # the item number is: QualityTreeview0.get_string_from_iter(QualityTreeview1)
		QualityStr=str(QualityTreeview0.get_value(QualityTreeview1,0))
		QualityStr=QualityStr.partition('(')[2] # the quality is stored between (  ) in the string...
		QualityStr=QualityStr.rpartition(')')[0]
		return QualityStr
	
	def on_SizeTreeviewSel_changed(self, widget):
		self.SizeSelectedLabel.set_text('Current selected size: '+self.get_size())
	
	def on_QualityTreeviewSel_changed(self, widget):
		self.QualitySelectedLabel.set_text('Current selected quality: '+self.get_quality())
	
	def on_CancelButton_clicked(self, widget):
		self.message('Operation canceled',level=1)
		self.message('Quit Jivaro',level=1)
		gtk.main_quit()
	
	def on_OkButton_clicked(self, widget):
		self.message('Operation Launched with selected options:',level=2)
		#
		command_str=os.path.join(self.directory,"jivaro.py")
		options_str=''
		# size
		sizeFlag=False
		if self.SizeTreeview.get_selection().count_selected_rows()==1:
			(SizeTreeview0,SizeTreeview1)=self.SizeTreeview.get_selection().get_selected() # the item number is: SizeTreeview0.get_string_from_iter(SizeTreeview1)
			SizeStr=str(SizeTreeview0.get_value(SizeTreeview1,0))
			if SizeStr.isdigit():
				self.message('Size selection: '+SizeStr,level=2)
				options_str+=' --size='+SizeStr
				sizeFlag=True
			else:
				self.message('Size canot be read from string (SizeTreeview)',level=1)
		# quality
		qualityFlag=False
		if self.QualityTreeview.get_selection().count_selected_rows()==1:
			(QualityTreeview0,QualityTreeview1)=self.QualityTreeview.get_selection().get_selected() # the item number is: QualityTreeview0.get_string_from_iter(QualityTreeview1)
			QualityStr=str(QualityTreeview0.get_value(QualityTreeview1,0))
			QualityStr=QualityStr.partition('(')[2] # the quality is stored between (  ) in the string...
			QualityStr=QualityStr.rpartition(')')[0]
			if QualityStr.isdigit():
				self.message('Quality selection: '+QualityStr,level=2)
				options_str+=' --quality='+QualityStr
				qualityFlag=True
			else:
				self.message('Quality canot be read from string (QualityTreeview)',level=1)
		if sizeFlag and qualityFlag:
			# prefixes
			prefix_string_flag=True
			if self.SeparatorCheckbutton.get_active():
				if not self.SizeCheckbutton.get_active() and not self.QualityCheckbutton.get_active():
					prefix_string_flag=False
				else:
					self.message('Prefix: '+self.SeparatorEntry_Text,level=2)
					options_str+=' --prefix_string='+self.SeparatorEntry_Text
					if self.SizeCheckbutton.get_active():
						self.message('Size: '+self.SizeEntry_Text,level=2)
						options_str+=' --size_string='+self.SizeEntry_Text
					else:
						self.message('Size in filename ignored',level=2)
						options_str+=' --size_string=NONE'
					if self.QualityCheckbutton.get_active():
						self.message('Quality: '+self.QualityEntry_Text,level=2)
						options_str+=' --quality_string='+self.QualityEntry_Text
					else:
						self.message('Quality in filename ignored',level=2)
						options_str+=' --quality_string=NONE'
			else:
				prefix_string_flag=False
			if prefix_string_flag==False:
				self.message('Prefix ignored',level=2)
				options_str+=' --prefix_string=NONE'
			# Brightness Contrast Color
			if not self.BrightnessSpinbutton.get_value()==1.0:
				options_str+=' --brightness={value}'.format(value=self.BrightnessSpinbutton.get_value())
			if not self.ContrastSpinbutton.get_value()==1.0:
				options_str+=' --contrast={value}'.format(value=self.ContrastSpinbutton.get_value())
			if not self.ColorSpinbutton.get_value()==1.0:
				options_str+=' --color={value}'.format(value=self.ColorSpinbutton.get_value())
			# prefixes
			self.message('Overwrite: '+str(self.OverwriteCheckbutton.get_active()),level=2)
			if self.OverwriteCheckbutton.get_active():
				options_str+=' --overwrite'
			self.message('Scale up: '+str(self.ScaleUpCheckbutton.get_active()),level=2)
			if self.ScaleUpCheckbutton.get_active():
				options_str+=' --scaleup'
			# args
			options_str3=''
			for arg in self.args:
				if isinstance(arg,str):
					options_str3+=' "'+arg+'"'
			options_str+=options_str3
			# launch Jivaro in command mode with options
			self.message('Run Jivaro',level=2)
			self.message(command_str+options_str,level=1)
			os.system(command_str+options_str)
		else:
			self.message('Jivaro cannot run because the size and/or the quality are not present',level=0)
		gtk.main_quit()
	
	def on_SeparatorEntry_changed(self, widget):
		self.SeparatorEntry_Text=self.SeparatorEntry.get_text()
	
	def on_SizeEntry_changed(self, widget):
		self.SizeEntry_Text=self.SizeEntry.get_text()
	
	def on_QualityEntry_changed(self, widget):
		self.QualityEntry_Text=self.QualityEntry.get_text()


if __name__ == "__main__":
	jivaro=JivaroUI(sys.argv[1:])
	gtk.main()
